package com.example;

import com.example.domain.Persona;
import com.example.repository.PersonaRepository;
import java.util.stream.Stream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

    @Autowired
    private PersonaRepository personaRepository;

    @Test
    public void filtremos() {

        Stream<Persona> personas = personaRepository.findAll()
                .stream();

        System.out.println(personas.count());
    }

    @Test
    public void filtremosPeroPrediquemosMas() {

        Stream<Persona> personas = personaRepository.findAll()
                .stream();
        
        System.out.println(personas.count());
    }

}
